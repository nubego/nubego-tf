variable "aws_provider_profile" {
  description = "AWS profile to use for the AWS provider"
  default     = ""
}

variable "aws_provider_region" {
  description = "AWS region to use for the AWS provider"
  default = ""
}

variable "saml_provider_name" {
  description = "Name to be assigned to the IAM SAML provider"
  default     = ""
}

variable "aws_provider_assume_role_arn" {
  description = "ARN of role to assume for the AWS provider"
  default     = ""
}

variable "saml_metadata_document" {
  description = "Location of Metadata file to be uploaded"
  default     = ""
}
