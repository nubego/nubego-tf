provider "aws" {
  alias   = "module"
  profile = "${var.aws_provider_profile}"
  region  = "${var.aws_provider_region}"

  assume_role {
    role_arn = "${var.aws_provider_assume_role_arn}"
  }

}

resource "aws_iam_saml_provider" "default" {
  provider               = "aws.module"
  name                   = "${var.saml_provider_name}"
  saml_metadata_document = "${file("${var.saml_metadata_document}")}"
}
