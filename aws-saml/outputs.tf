output "aws_iam_saml_provider_arn" {
  value = "${aws_iam_saml_provider.default.*.arn}"
}
