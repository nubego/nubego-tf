#cloudadmin
output "cloudadmin_iam_role_arn" {
  description = "ARN of cloudadmin IAM role"
  value       = "${element(concat(aws_iam_role.cloudadmin.*.arn, list("")), 0)}"
}

output "cloudadmin_iam_role_name" {
  description = "Name of cloudadmin IAM role"
  value       = "${element(concat(aws_iam_role.cloudadmin.*.name, list("")), 0)}"
}

output "cloudadmin_iam_role_path" {
  description = "Path of cloudadmin IAM role"
  value       = "${element(concat(aws_iam_role.cloudadmin.*.path, list("")), 0)}"
}

output "devops_iam_role_arn" {
  description = "ARN of devops IAM role"
  value       = "${element(concat(aws_iam_role.devops.*.arn, list("")), 0)}"
}

output "devops_iam_role_name" {
  description = "Name of devops IAM role"
  value       = "${element(concat(aws_iam_role.devops.*.name, list("")), 0)}"
}

output "devops_iam_role_path" {
  description = "Path of devops IAM role"
  value       = "${element(concat(aws_iam_role.devops.*.path, list("")), 0)}"
}

# developer
output "developer_iam_role_arn" {
  description = "ARN of developer IAM role"
  value       = "${element(concat(aws_iam_role.developer.*.arn, list("")), 0)}"
}

output "developer_iam_role_name" {
  description = "Name of developer IAM role"
  value       = "${element(concat(aws_iam_role.developer.*.name, list("")), 0)}"
}

output "developer_iam_role_path" {
  description = "Path of developer IAM role"
  value       = "${element(concat(aws_iam_role.developer.*.path, list("")), 0)}"
}
