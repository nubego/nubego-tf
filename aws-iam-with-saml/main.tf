provider "aws" {
  alias   = "module"
  profile = "${var.aws_provider_profile}"
  region  = "${var.aws_provider_region}"

  assume_role {
    role_arn = "${var.aws_provider_assume_role_arn}"
  }

}

data "aws_iam_policy_document" "assume_role_with_saml" {
  statement {
    effect = "Allow"

    actions = ["sts:AssumeRoleWithSAML"]

    principals {
      type        = "Federated"
      identifiers = ["${var.provider_id}"]
    }

    condition {
      test     = "StringEquals"
      variable = "SAML:aud"
      values   = ["${var.aws_saml_endpoint}"]
    }
  }
}

# cloudadmin
resource "aws_iam_role" "cloudadmin" {
  provider             = "aws.module"
  count = "${var.create_cloudadmin_role ? 1 : 0}"
  name                 = "${var.project}-${var.environment}-cloudadmin"
  description          = "${var.iam_role_cloudadmin_description}"
  path                 = "${var.cloudadmin_role_path}"
  max_session_duration = "${var.max_session_duration}"

  permissions_boundary = "${var.cloudadmin_role_permissions_boundary_arn}"

  assume_role_policy = "${data.aws_iam_policy_document.assume_role_with_saml.json}"
}

resource "aws_iam_role_policy_attachment" "cloudadmin" {
  provider               = "aws.module"
  count = "${var.create_cloudadmin_role ? 1 : 0}"
  role       = "${var.project}-${var.environment}-cloudadmin"
  policy_arn = "${var.cloudadmin_role_policy_arn}"
}

# devops
resource "aws_iam_role" "devops" {
  provider               = "aws.module"
  count = "${var.create_devops_role ? 1 : 0}"
  name                 = "${var.project}-${var.environment}-devops"
  description          = "${var.iam_role_devops_description}"
  path                 = "${var.devops_role_path}"
  max_session_duration = "${var.max_session_duration}"

  permissions_boundary = "${var.devops_role_permissions_boundary_arn}"

  assume_role_policy = "${data.aws_iam_policy_document.assume_role_with_saml.json}"
}

resource "aws_iam_role_policy_attachment" "devops" {
  provider               = "aws.module"
  count = "${var.create_devops_role ? 1 : 0}"
  role       = "${var.project}-${var.environment}-devops"
  policy_arn = "${var.devops_role_policy_arn}"
}

# Developer
resource "aws_iam_role" "developer" {
  provider               = "aws.module"
  count = "${var.create_developer_role ? 1 : 0}"
  name                 = "${var.project}-${var.environment}-developer"
  description          = "${var.iam_role_developer_description}"
  path                 = "${var.developer_role_path}"
  max_session_duration = "${var.max_session_duration}"

  permissions_boundary = "${var.developer_role_permissions_boundary_arn}"

  assume_role_policy = "${data.aws_iam_policy_document.assume_role_with_saml.json}"
}

resource "aws_iam_role_policy_attachment" "developer" {
  provider               = "aws.module"
  count = "${var.create_developer_role ? 1 : 0}"
  role       = "${var.project}-${var.environment}-developer"
  policy_arn = "${var.developer_role_policy_arn}"
}
