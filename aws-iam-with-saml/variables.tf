variable "environment" {
  description = "Name of the account environment"
}

variable "project" {
  description = "Name of the project"
}

variable "provider_id" {
  description = "ID of the SAML Provider"
  type = "list"
}

variable "aws_provider_profile" {
  description = "AWS profile to use for the AWS provider"
  default     = ""
}

variable "aws_provider_region" {
  description = "AWS region to use for the AWS provider"
  default = ""
}

variable "aws_provider_assume_role_arn" {
  description = "ARN of role to assume for the AWS provider"
  default     = ""
}

variable "aws_saml_endpoint" {
  description = "AWS SAML Endpoint"
  default     = ["https://signin.aws.amazon.com/saml"]
  type        = "list"
}

# cloudadmin
variable "create_cloudadmin_role" {
  description = "Whether to create cloudadmin role"
  default     = false
}

variable "iam_role_cloudadmin_description" {
  description = "Text to add to role description field"
  default = "Role to provide access to Administrators"
}

variable "cloudadmin_role_path" {
  description = "Path of cloudadmin IAM role"
  default     = "/"
}

variable "cloudadmin_role_policy_arn" {
  description = "Policy ARN to use for cloudadmin role"
  default     = "arn:aws:iam::aws:policy/administratorAccess"
}

variable "cloudadmin_role_permissions_boundary_arn" {
  description = "Permissions boundary ARN to use for admin role"
  default     = ""
}

# DevOps
variable "create_devops_role" {
  description = "Whether to create devops role"
  default     = false
}

variable "iam_role_devops_description" {
  description = "Text to add to role description field"
  default = "Role to provide access to DevOps users"
}

variable "devops_role_path" {
  description = "Path of devops IAM role"
  default     = "/"
}

variable "devops_role_policy_arn" {
  description = "Policy ARN to use for devops role"
  default     = "arn:aws:iam::aws:policy/PowerUserAccess"
}

variable "devops_role_permissions_boundary_arn" {
  description = "Permissions boundary ARN to use for devops role"
  default     = ""
}

# Developer
variable "create_developer_role" {
  description = "Whether to create developer role"
  default     = false
}

variable "iam_role_developer_description" {
  description = "Text to add to role description field"
  default = "Role to provide access to developers"
}

variable "developer_role_path" {
  description = "Path of developer IAM role"
  default     = "/"
}

variable "developer_role_policy_arn" {
  description = "Policy ARN to use for developer role"
  default     = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

variable "developer_role_permissions_boundary_arn" {
  description = "Permissions boundary ARN to use for developer role"
  default     = ""
}

variable "max_session_duration" {
  description = "Maximum CLI/API session duration in seconds between 3600 and 43200"
  default     = 3600
}
