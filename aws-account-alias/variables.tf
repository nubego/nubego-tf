variable "account_alias" {
  description = "Display name for the account"
}

variable "aws_provider_profile" {
  description = "AWS profile to use for the AWS provider"
  default     = ""
}

variable "aws_provider_region" {
  description = "AWS region to use for the AWS provider"
  default = ""
}

variable "aws_provider_assume_role_arn" {
  description = "ARN of role to assume for the AWS provider"
  default     = ""
}
