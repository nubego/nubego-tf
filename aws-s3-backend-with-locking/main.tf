resource "aws_kms_key" "s3_tf_state_key" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
}

resource "aws_s3_bucket" "terraform-state-files" {
  bucket = "${var.s3_bucket}"
  acl    = "private"
  tags {
    Name        = "${var.s3_bucket_name}"
    Environment = "${var.environment}"
  }

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${aws_kms_key.s3_tf_state_key.arn}"
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform" {
  name           = "${var.dynamodb_table}"
  read_capacity  = 10
  write_capacity = 10
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name        = "Terraform Lock Table"
    Environment = "${var.environment}"
    Project     = "${var.project}"
  }
}
