variable "environment" {
  description = "Name of the account environment"
}

variable "project" {
  description = "Name of the client"
}

variable "s3_bucket" {
  description = "Name of the S3 bucket"
}

variable "s3_bucket_name" {
  description = "'Name' tag for S3 bucket with terraform state"
}

variable "dynamodb_table" {
  description = "Name of the DynamoDB table for the terraform lock."
}
