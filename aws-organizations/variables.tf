variable "account_name" {
  type        = "list"
  description = "Friendly name for the member account"
  default     = [""]
}

variable "account_email" {
  type        = "list"
  description = "The email address of the owner to assign to the new member account. This email address must not already be associated with another AWS account."
  default     = [""]
}

variable "project" {
  default     = ""
}
