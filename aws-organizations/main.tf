resource "aws_organizations_account" "account" {
  count = "${length(var.account_name)}"
  name  = "${var.account_name[count.index]}"
  email = "${var.account_email[count.index]}"
}
